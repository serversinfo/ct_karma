#pragma semicolon 1

#include <t2lrcfg>
#pragma newdecls required
#define VERSION "1.0.3"

public Plugin myinfo = 
{
	name		= "[JAIL] CT Karma",
	author		= "ShaRen",
	description = "Players can evaluate CT",
	version		= VERSION,
	url			= "Servers-Info.Ru"
}

stock void debugMessage(const char[] message, any ...)
{
	char szMessage[256], szPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, szPath, sizeof(szPath), "logs/jail_ct_karma.txt");
	VFormat(szMessage, sizeof(szMessage), message, 2);
	LogToFile(szPath, szMessage);
}
#define dbgMsg(%0) debugMessage(%0)

// макросы
// получить userid из index
#define UID(%0) GetClientUserId(%0)
// получить index из userid
#define CID(%0) GetClientOfUserId(%0)
// проверка времени при нажатии в SQL, вернуть Plugin_Handled (для команд)
#define CTH(%0) int iTime=GetTime(); if (iTime<g_iSQLTime[%0]) {g_iSQLTime[%0]=iTime+2; PrintToChat(%0, "Интервал запроса 2 секунды"); return Plugin_Handled;}
// проверка времени при нажатии в SQL, вернуть void (для функций)
#define CT(%0) int iTime=GetTime(); if (iTime<g_iSQLTime[%0]) {g_iSQLTime[%0]=iTime+2; PrintToChat(%0, "Интервал запроса 2 секунды"); return;}


Database	g_hDatabase;						// подключение к SQL
int g_iVoteTime[MAXPLAYERS+1][MAXPLAYERS+1];	// когда игрок 1 проголосовал за игрок2
//int			g_iSQLTime[MAXPLAYERS+1];		// время последнего запроса игрока
int g_iPlus[MAXPLAYERS+1] = {-1, ...};
int g_iMinus[MAXPLAYERS+1] = {-1, ...};


public void OnPluginStart() 
{
	// подключение к локальной SQL
	char szError[256];
	g_hDatabase = SQLite_UseDatabase("jail_ct_karma", szError, sizeof(szError));
	if ( g_hDatabase == null ) {
		LogError("SQLite_UseDatabase error: \"%s\"", szError);
		SetFailState("SQLite_UseDatabase error: \"%s\"", szError);
	}
	// создание таблиц
	SQL_LockDatabase(g_hDatabase);
	SQL_FastQuery(g_hDatabase, "CREATE TABLE IF NOT EXISTS `ct_karma` (`id` INT, `steamid` VARCHAR(64), `playername` VARCHAR(64), `plus` INT, `minus` INT);");
	SQL_UnlockDatabase(g_hDatabase);
	for(int i=1; i<=MaxClients; i++)
		if(IsClientInGame(i))
			OnClientPutInServer(i);
	RegConsoleCmd("sm_karma", 		Command_Karma);
	RegConsoleCmd("sm_allct", 		Command_AllCt);
	RegConsoleCmd("sm_karmatop", 	Command_KarmaTop);
}

public void t2lrcfg_OnLrStarted()
{
	CreateTimer(5.0, Msg);
}

public Action Msg(Handle h)
{
	PrintToChatAll("Если Вам понравилось командование КТ, то пишите в чат \"!allct +\"");
}

// при вызове команды sm_karma
public Action Command_Karma(int iClient, int iArgs)
{
	PrintToConsole(iClient, "sm_karma ;)");
	Menu menu = CreateMenu(KarmaMenu_Callback, MenuAction_Select | MenuAction_End | MenuAction_DisplayItem);
	
	char sInfoString[64];
	char sDisplayString[64];
	SetMenuTitle(menu, "Оценить КТ");
	int iCount = 0;
	for (int i=1; i<=MaxClients; i++)
		if (IsValidClient(i) && GetClientTeam(i) == 3 && i != iClient) {
			Format(sInfoString, sizeof(sInfoString), "%i", GetClientUserId(i));
			Format(sDisplayString, sizeof(sDisplayString), "%N", i);
			menu.AddItem(sInfoString, sDisplayString, (GetTime() - g_iVoteTime[iClient][i] < 60)?ITEMDRAW_DISABLED:ITEMDRAW_DEFAULT);
			iCount++;
		}
	menu.AddItem("top", "Рейтинг игроков", ITEMDRAW_DEFAULT);
	menu.Display(iClient, MENU_TIME_FOREVER);
	PrintToChat(iClient, "Твой рейтинг %d плюсов и %d минусов", g_iPlus[iClient], g_iMinus[iClient]);
}

// при вызове команды sm_allct
public Action Command_AllCt(int iClient, int iArgs)
{
	if (iArgs < 1) {
		ReplyToCommand(iClient, "[SM] Usage: sm_allct <+ or ->");
		if(iClient)
			PrintToChat(iClient, "\"!allct +\" поставить всем КТ плюсы, \"!allct -\"поставить всем КТ минусы");
		return Plugin_Handled;
	}
	char sBuf[8];
	GetCmdArg(1, sBuf, sizeof(sBuf));
	bool bPlus;
	if (StrEqual(sBuf, "+"))
		bPlus = true;
	else if (!StrEqual(sBuf, "-")) {
		ReplyToCommand(iClient, "[SM] Usage: sm_allct <+ or ->");
		if(iClient)
			PrintToChat(iClient, "\"!allct +\" поставить всем КТ плюсы, \"!allct -\"поставить всем КТ минусы");
		return Plugin_Handled;
	}
	for(int i=1; i<=MaxClients;i++)
		if(IsClientInGame(i) && GetClientTeam(i) == 3 && i!=iClient && GetTime() - g_iVoteTime[iClient][i] > 60) {
			g_iVoteTime[iClient][i] = GetTime();
			char szQuery[512], szAuth[32];
			GetClientAuthId(i, AuthId_Steam2, szAuth, sizeof(szAuth));
			if(bPlus) {
				PrintToChatAll("%N оценил %N положительно", iClient, i);
				dbgMsg("%N оценил %N положительно. теперь %i плюсов", iClient, i, g_iPlus[i]+1);
				if(g_iPlus[i] == 0 && g_iMinus[i] == 0)
					FormatEx(szQuery, sizeof(szQuery), "INSERT INTO `ct_karma` (`id`, `steamid`, `playername`, `plus`, `minus`) SELECT (SELECT IFNULL(MAX(id)+1, 1) FROM `ct_karma`), '%s', '%N', '1', '0' WHERE NOT EXISTS (SELECT `steamid` FROM `ct_karma` WHERE `steamid` = '%s');", szAuth, i, szAuth);
				else FormatEx(szQuery, sizeof(szQuery), "UPDATE `ct_karma` SET `plus` = plus+1 WHERE `steamid` = '%s'", szAuth);
				g_iPlus[i]++;
			} else {
				PrintToChatAll("%N оценил %N отрицательно", iClient, i);
				dbgMsg("%N оценил %N отрицательно. теперь %i минусов", iClient, i, g_iMinus[i]+1);
				if(g_iPlus[i] == 0 && g_iMinus[i] == 0)
					FormatEx(szQuery, sizeof(szQuery), "INSERT INTO `ct_karma` (`id`, `steamid`, `playername`, `plus`, `minus`) SELECT (SELECT IFNULL(MAX(id)+1, 1) FROM `ct_karma`), '%s', '%N', '0', '1' WHERE NOT EXISTS (SELECT `steamid` FROM `ct_karma` WHERE `steamid` = '%s');", szAuth, i, szAuth);
				else FormatEx(szQuery, sizeof(szQuery), "UPDATE `ct_karma` SET `minus` = minus+1 WHERE `steamid` = '%s'", szAuth);
				g_iMinus[i]++;
			} 
			dbgMsg(szQuery);
			g_hDatabase.Query(SQLT_SaveKarma, szQuery);
		}
	return Plugin_Continue;
}

public void OnClientPutInServer(int iClient)
{	// получаем стим, отправляем запрос на очки

	g_iPlus[iClient] = -1;
	g_iMinus[iClient] = -1;
	char szAuth[32];
	GetClientAuthId(iClient, AuthId_Steam2, szAuth, sizeof(szAuth));
	dbgMsg("%N подключился (%s) (OnClientPutInServer)", iClient, szAuth);
	char szQuery[256];
	FormatEx(szQuery, sizeof(szQuery), "SELECT `plus`, `minus` FROM `ct_karma` WHERE `steamid` = '%s' LIMIT 1;", szAuth);
	g_hDatabase.Query(SQLT_OnClientPutInServer, szQuery, GetClientUserId(iClient));
}

public void SQLT_OnClientPutInServer(Database hDatabase, DBResultSet hQuery, const char[] szError, any iUserId)
{	// при получении ответа от бд на данные игрока
	if ( hQuery == null )
		LogError("SQLT_OnClientPutInServer error: \"%s\"", szError);
	// если данные есть, то получаем, иначе вставляем нули в бд
	int iClient = GetClientOfUserId(iUserId);
	if (iClient) {
		if ( hQuery.FetchRow() ) {
			g_iPlus[iClient] = hQuery.FetchInt(0);
			g_iMinus[iClient] = hQuery.FetchInt(1);
			dbgMsg("ответ с БД g_iPlus[%N] - %i g_iMinus[%N] - %i", iClient, g_iPlus[iClient], iClient, g_iMinus[iClient]);
		} else {
			dbgMsg("У %N еще нет оценок", iClient);
			g_iPlus[iClient] = 0;
			g_iMinus[iClient] = 0;
		}
	} else dbgMsg("ОШИБКА ответ с БД пришел, но игрок вышел");
}

public int KarmaMenu_Callback(Menu menu, MenuAction action, int iClient, int param2)
{
	switch (action) {
		case MenuAction_Select: {
			char sInfo[64];
			GetMenuItem(menu, param2, sInfo, sizeof(sInfo));
			if(StrEqual(sInfo, "top")){
				Command_KarmaTop(iClient, 0);
				return;
			}
			int iTarget = GetClientOfUserId(StringToInt(sInfo));
			if (GetTime() - g_iVoteTime[iClient][iTarget] > 60) {

				PrintToConsole(iClient, "sm_karma =o");
				Menu menu2 = CreateMenu(KarmaMenu2_Callback, MenuAction_Select | MenuAction_End | MenuAction_DisplayItem);
				SetMenuTitle(menu2, "Оценить КТ");
				menu2.AddItem(sInfo, "Поставить +", ITEMDRAW_DEFAULT);
				menu2.AddItem(sInfo, "Поставить -", ITEMDRAW_DEFAULT);
				menu2.Display(iClient, MENU_TIME_FOREVER);
			} else PrintToChat(iClient, "Вы сможете проголосовать за игрока %N через %i секунд", iTarget, 60 - (GetTime() - g_iVoteTime[iClient][iTarget]));
		}
		case MenuAction_Cancel: {}
		case MenuAction_End: delete menu;
	}
	return;
}

public int KarmaMenu2_Callback(Menu menu, MenuAction action, int iClient, int choise)
{
	switch (action) {
		case MenuAction_Select: {
			char sInfo[64];
			GetMenuItem(menu, choise, sInfo, sizeof(sInfo));
			int iTarget = GetClientOfUserId(StringToInt(sInfo));
			g_iVoteTime[iClient][iTarget] = GetTime();
			char szQuery[512], szAuth[32];
			GetClientAuthId(iTarget, AuthId_Steam2, szAuth, sizeof(szAuth));
			if (choise == 0) {
				PrintToChatAll("%N оценил %N положительно", iClient, iTarget);
				dbgMsg("%N оценил %N положительно. теперь %i плюсов", iClient, iTarget, g_iPlus[iTarget]+1);

				if(g_iPlus[iTarget] == 0 && g_iMinus[iTarget] == 0)
					FormatEx(szQuery, sizeof(szQuery), "INSERT INTO `ct_karma` (`id`, `steamid`, `playername`, `plus`, `minus`) SELECT (SELECT IFNULL(MAX(id)+1, 1) FROM `ct_karma`), '%s', '%N', '1', '0' WHERE NOT EXISTS (SELECT `steamid` FROM `ct_karma` WHERE `steamid` = '%s');", szAuth, iTarget, szAuth);
				else FormatEx(szQuery, sizeof(szQuery), "UPDATE `ct_karma` SET `plus` = plus+1 WHERE `steamid` = '%s'", szAuth);
				g_iPlus[iTarget]++;
			} else if (choise == 1) {
				PrintToChatAll("%N оценил %N отрицательно", iClient, iTarget);
				dbgMsg("%N оценил %N отрицательно. теперь %i минусов", iClient, iTarget, g_iMinus[iTarget]+1);

				if(g_iPlus[iTarget] == 0 && g_iMinus[iTarget] == 0)
					FormatEx(szQuery, sizeof(szQuery), "INSERT INTO `ct_karma` (`id`, `steamid`, `playername`, `plus`, `minus`) SELECT (SELECT IFNULL(MAX(id)+1, 1) FROM `ct_karma`), '%s', '%N', '0', '1' WHERE NOT EXISTS (SELECT `steamid` FROM `ct_karma` WHERE `steamid` = '%s');", szAuth, iTarget, szAuth);
				else FormatEx(szQuery, sizeof(szQuery), "UPDATE `ct_karma` SET `minus` = minus+1 WHERE `steamid` = '%s'", szAuth);
				g_iMinus[iTarget]++;
			}
			dbgMsg(szQuery);
			g_hDatabase.Query(SQLT_SaveKarma, szQuery);
		}
		case MenuAction_Cancel: {}
		case MenuAction_End: delete menu;
	}
	return;
}

// при отключении клиента, SQL запрос
public void SQLT_SaveKarma(Database hDatabase, DBResultSet hQuery, const char[] szError, any iUserId)
{
	if ( hQuery == null )
		LogError("SQLT_SaveKarma error: \"%s\"", szError);
}



// создать топ
public Action Command_KarmaTop(int iClient, int iArgc)
{
	g_hDatabase.Query(SQLT_OnGetTop, "SELECT `plus`, `minus`, `playername` FROM `ct_karma` ORDER BY `plus` DESC LIMIT 50;", UID(iClient));
}

// при получении данных для топа от бд
public void SQLT_OnGetTop(Database hDatabase, DBResultSet hQuery, const char[] szError, any iUserId)
{
	if (hQuery == null)
		LogError("SQLT_OnGetTop error: \"%s\"", szError);
	
	// проверка, на сервере ли запросивший клиент
	int iClient = CID(iUserId);
	if (!iClient) return;
	
	// создание меню
	Menu hMenu = new Menu(Handle_Top);
	hMenu.SetTitle("Топ");
	
	// пока есть данные, добавление в меню
	while ( hQuery.FetchRow() ) {
		int iPlus, iMinus; char szName[32], szBuffer[64];
		iPlus = hQuery.FetchInt(0);
		iMinus = hQuery.FetchInt(1);
		hQuery.FetchString(2, szName, sizeof(szName));
		FormatEx(szBuffer, sizeof(szBuffer), "%s (+%d/-%d)", szName, iPlus, iMinus);
		hMenu.AddItem(NULL_STRING, szBuffer, ITEMDRAW_DISABLED);
	}
	
	// вдруг в бд ничего нет по запросу
	if ( !hMenu.ItemCount )
		hMenu.AddItem(NULL_STRING, "Пусто", ITEMDRAW_DISABLED);
	
	// добавление кнопки "назад", вывод клиенту меню
	hMenu.ExitBackButton = true;
	hMenu.Display(iClient, MENU_TIME_FOREVER);
}

// при натажии в топ меню 
public int Handle_Top(Menu hMenu, MenuAction action, int iClient, int iSlot)
{
	if ( action == MenuAction_Cancel && iSlot == MenuCancel_ExitBack )
		Command_Karma(iClient, 0);
	else if ( action == MenuAction_End )
		delete hMenu;
}


bool IsValidClient(int client)
{
	if (!(1 <= client <= MaxClients) || !IsClientInGame(client) || IsClientSourceTV(client) || IsClientReplay(client))
		return false;
	return true;
}